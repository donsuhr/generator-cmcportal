# CMC Portal Yeoman Generator

Yeoman generator for creating CMC Portal project modules. Contains the following bower installed dependencies.

* [Grunt](http://gruntjs.com/)
* [Bower](http://bower.io/)
* [Twitter Bootstrap](http://getbootstrap.com/2.3.2/)
* [jQuery](http://jquery.com/)
* [Mockjax](https://github.com/appendto/jquery-mockjax) & [mockJSON](http://experiments.mennovanslooten.nl/2010/mockjson/)
* [Microsoft datajs](http://datajs.codeplex.com/)
* [jquery-placeholder](https://github.com/mathiasbynens/jquery-placeholder)
* [Kendo UI](http://www.kendoui.com/)
* [LESS Hat](http://lesshat.com/)
* [RequireJS](http://requirejs.org/)
* [RequireJS text plugin](https://github.com/requirejs/text)
* [CMC portal-core](https://bitbucket.org/zorg128/portal-core)

**[Install](#markdown-header-install)**
 
**[Use](#markdown-header-use)**
 
**[Preview](#markdown-header-preview)**
 
---

##Install

### Install Node
Check to see if node is installed.

`node —version`

If not, download and install it. [http://nodejs.org/download/](http://nodejs.org/download/)

### Install Git
Check to see if git is installed.

`git —version`

If not, download and install it. [http://git-scm.com/downloads](http://git-scm.com/downloads)


### Install  Yeoman
Check to see if Yeoman is installed.

`npm list -g | grep yo`

If not, install it.

`npm install -g yo`

### Install the Yeoman CMC Portal generator
Check to see if the generator is already installed.

`npm list -g | grep cmcportal`

If not, install it.

`npm install -g git+https://bitbucket.org/zorg128/generator-cmcportal.git`

---

##Use
Create a working directory for your module.

`mkdir myProject`

Change directory to your new folder.

`cd myProject`

Create a new project with the generator.

`yo cmcportal`

Follow the prompts to complete the generation of the project. If the bower install does not run automatically, execute the following command.

`bower install & npm install`

---

##Preview
The following example demonstrates a trivial use of the Grunt `preview` task. At the command prompt, issue the following command: 

`grunt preview`

After running the [JSHint](http://www.jshint.com/) and [QUnit](http://qunitjs.com/) tasks grunt will open a browser and hit the project files.

To see the live preview, change one of the less files. For example, in the `app/styles/[project name]` folder exists a file `[project name].less`. After changing and saving the contents of this file the grunt `watch` task will recompile the LESS files and reload the CSS file in the open browser.
