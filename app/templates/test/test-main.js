var allTestFiles = [];
var TEST_REGEXP = /(spec|test)\.js$/i;

var pathToModule = function (path) {
	'use strict';
	return path.replace(/^\/base\//, '').replace(/\.js$/, '');
};

Object.keys(window.__karma__.files).forEach(function (file) {
	'use strict';
	if (TEST_REGEXP.test(file)) {
		// Normalize paths to RequireJS module names.
		allTestFiles.push(pathToModule(file));

	}

});

require(['/base/app/bower_components/portal-core/app/scripts/common.js'], function () {
	require.config({
		// Karma serves files under /base, which is the basePath from your config file
		baseUrl: '/base/app/scripts',
		paths: {
			'test': '../../test',
			'jasmine-jquery': '../bower_components/jasmine-jquery/lib/jasmine-jquery'
		},
		callback: function () {
			'use strict';
			require(['jasmine-jquery'], function () {
				window.__karma__.start();
			});
		},
		shim: {
			'jasmine-jquery': {
				deps: ['jquery']
			}
		},
		// dynamically load all test files
		deps: allTestFiles
	});
});

