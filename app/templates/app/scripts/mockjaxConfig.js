/*
 configure mockjax and mockjson plugins
 */
define(['jquery', 'mockjax', 'mockjson'], function ($) {
	'use strict';
	/* jshint -W101:true */
	/* jshint -W109:true */
	$('#previewModeNotes').append('jQuery ' + $.fn.jquery + ' loaded!');
});