module.exports = function (grunt) {
	var _ = require('lodash');
	var current = grunt.config.get('requirejs');

	var packageFolder = grunt.config.get('packageFolder');
	var pkg = grunt.config.get('pkg');
	var moduleName = packageFolder + '/' + pkg.name;

	// get current module
	var moduleItem = _.findWhere(current.compile.options.modules, {name: moduleName });
	moduleItem.include = [packageFolder + '/<%%= pkg.name %>Options-cmc'];
	// remove current
	current.compile.options.modules =
		_.without(current.compile.options.modules, _.findWhere(current.compile.options.modules, {name: moduleName}));
	// replace
	current.compile.options.modules.push(moduleItem);

	grunt.config.set('requirejs', current);
};