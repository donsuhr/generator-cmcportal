define(function (require) {
	'use strict';

	var $ = require('jquery');
	require('<%= packageName %>/<%= appName %>');
	require('bootstrap');
	require('kendo/kendo.datetimepicker.min');
	require('kendo/kendo.tabstrip.min');

	$('#datetimepicker').kendoDateTimePicker({
		value: new Date()
	});
	$('#tabstrip').kendoTabStrip({
		animation: {
			open: {
				effects: 'fadeIn'
			}
		}
	});
	$('body').append('jQuery ' + $.fn.jquery + ' loaded!');
});
