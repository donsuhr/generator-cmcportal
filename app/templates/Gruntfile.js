'use strict';
module.exports = function (grunt) {
	require('load-grunt-tasks')(grunt, {
		pattern: ['grunt-*', '!grunt-template-jasmine-requirejs']
	});
	require('time-grunt')(grunt);

	var bowerrc = grunt.file.readJSON('./.bowerrc');
	var bowerDir = bowerrc.directory;
	var coreModules = grunt.file.readJSON('./' + bowerDir + '/portal-core/coreIncludes.json');
	var pkg = grunt.file.readJSON('package.json');
	var packageFolder = pkg.name.charAt(0).toLowerCase() + pkg.name.slice(1);

	grunt.initConfig({
		pkg: pkg,
		packageFolder: packageFolder,
		bowerDir: bowerDir,
		coreModules: coreModules,
		root: __dirname,
		yeoman: {
			app: 'app',
			dist: 'dist'
		}
	});

	grunt.task.loadTasks('./' + bowerDir + '/portal-core/dist/build/grunt-module-base');
	//overrides
	grunt.task.loadTasks('build/grunt');

	grunt.registerTask('build', [
		'clean', 'less:dist', 'processhtml', 'useminPrepare', 'htmlmin',
		'autoprefixer', 'concat', 'cssmin', 'usemin', 'imagemin',
		'amdWrapKendoFiles', 'requirejs', 'copy:dist', 'uglify'
	]);

	grunt.registerTask('dist', ['serve']);

	grunt.registerTask('serve', ['build', 'connect:dist:keepalive']);

	grunt.registerTask('test',
		['qunit', 'jasmine', 'jshint', 'kendo_lint', 'complexity']);

	grunt.registerTask('preview', [
		'clean', 'less:dist', 'qunit', 'jshint', 'connect:livereload',
		'concurrent:preview'
	]);

	grunt.registerTask('work', [
		'clean', 'less:dist', 'connect:livereload', 'concurrent:working'
	]);

	grunt.registerTask('default', ['work']);
};
