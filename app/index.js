'use strict';
var util = require('util');
var path = require('path');
var yeoman = require('yeoman-generator');

var CmcportalGenerator = module.exports = function CmcportalGenerator(args, options, config) {
	yeoman.generators.Base.apply(this, arguments);
	this.options = options || {};
	this.on('end', function () {
		this.installDependencies({ skipInstall: options['skip-install'] });
	});

	this.pkg = JSON.parse(this.readFileAsString(path.join(__dirname, '../package.json')));
};

util.inherits(CmcportalGenerator, yeoman.generators.Base);

CmcportalGenerator.prototype.askFor = function askFor() {
	var cb = this.async();

	// have Yeoman greet the user.
	console.log(this.yeoman);

	var prompts = [
		{
			name: 'appname',
			message: 'What is the name of your app?'
			//default: 'testgit'
		}
	];

	this.prompt(prompts, function (props) {
		this.appName = props.appname;
		this.packageName = this.appname.charAt(0).toLowerCase() + this.appName.slice(1);
		cb();
	}.bind(this));
};

CmcportalGenerator.prototype.app = function app() {
	this.directory('app', 'app');
	this.directory('test', 'test');
	this.directory('build', 'build');
	this.mkdir('app/scripts/' + this.packageName);
	this.mkdir('app/images/' + this.packageName);
	this.mkdir('app/templates/' + this.packageName);
	this.mkdir('test/spec/');

	this.template('Gruntfile.js', 'Gruntfile.js');
	this.template('index.html', 'app/index.html');
	this.template('_bower.json', 'bower.json');
	this.template('_package.json', 'package.json');
	this.template('main.js', 'app/scripts/main.js');
	this.template('layout.tmpl.html', 'app/templates/' + this.packageName + '/layout.tmpl.html');

	this.copy('bowerrc', '.bowerrc');
	this.copy('gitignore', '.gitignore');
	this.copy('main.less', 'app/styles/' + this.packageName + '.less');
	this.copy('module.js', 'app/scripts/' + this.packageName + '/' + this.appName + '.js');
	this.copy('module.js', 'app/scripts/' + this.packageName + '/' + this.appName + 'Odata.js');
	this.copy('spec.js', 'test/spec/' + this.appName + 'Spec.js');
};

CmcportalGenerator.prototype.projectfiles = function projectfiles() {
	this.copy('editorconfig', '.editorconfig');
	this.copy('jshintrc', '.jshintrc');
};
